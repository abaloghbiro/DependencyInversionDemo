package hu.braininghub.bh06.ooptraining.di;

public class App {

	public static void main(String[] args) {

		PrinterService console = new ConsolePrinterService();
		PrinterService file = new FilePrinterService();

		Client c = null;

		if (args.length != 0 && args[0].equals("file")) {
			c = new Client(file);
		} else {
			c = new Client(console);
		}

		
		PrinterService service = c.getService();
		service.print("Hello World!");
		
		c.getService().print("Hello World!");
	}
}
