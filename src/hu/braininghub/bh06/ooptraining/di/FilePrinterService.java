package hu.braininghub.bh06.ooptraining.di;

public class FilePrinterService extends PrinterService {

	@Override
	public void print(String message) {
		System.out.println("File created with message: "+message);
	}

}
