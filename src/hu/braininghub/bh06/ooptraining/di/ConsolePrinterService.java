package hu.braininghub.bh06.ooptraining.di;

public class ConsolePrinterService extends PrinterService {

	
	public void print(String message) {
		System.out.println("Message printed: "+message);
	}
}
