package hu.braininghub.bh06.ooptraining.di;

public class Client {

	private PrinterService service;

	public PrinterService getService() {
		return service;
	}

	public Client(PrinterService service) {
		this.service = service;
	}

	
}
